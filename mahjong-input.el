;; -*- lexical-binding: t; -*-
(defconst manzulist
  '("🀇" "🀈" "🀉" "🀊" "🀋" "🀌" "🀍" "🀎" "🀏"))
(defconst souzulist
  '("🀐" "🀑" "🀒" "🀓" "🀔" "🀕" "🀖" "🀗" "🀘"))
(defconst pinzulist
  '("🀙" "🀚" "🀛" "🀜" "🀝" "🀞" "🀟" "🀠" "🀡"))
(defconst janpailist
  '(manzulist souzulist pinzulist))

(defconst janpai-type
  '(("m" . "萬子")
    ("s" . "索子")
    ("p" . "筒子")))

;;;###autoload
(defun mahjong-insert ()
  (interactive)
  (let ((interrupt nil)
        key
        (type "m")
        (current-hai-list (symbol-value (nth 0 janpailist))))
    (while (eq interrupt nil)
      (setq key (read-key-sequence
                 (format
                  "[%s]を選択中: 1-9で牌を入力．m, s, pで牌の種類を選択,qで終了"
                  (cond
                   ((assoc type janpai-type) (cdr (assoc type janpai-type)))
                   (t "エラー")))))
      (cond
       ((< 0 (string-to-number key))
        (insert (nth
                 (1- (string-to-number key))
                 current-hai-list)))
       ((member key '("m" "s" "p"))
        (setq type key)
        (setq current-hai-list
              (symbol-value (nth
                             (- (length '("m" "s" "p"))
                                (length (member key '("m" "s" "p"))))
                             janpailist))))
       ((equal key "q")
        (setq interrupt t)
        (message "終了しました"))))))

(provide 'mahjong-input)
